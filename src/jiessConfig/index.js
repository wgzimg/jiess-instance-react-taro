import { View } from '@tarojs/components';
import React from 'react';

import { $entry } from '@jiess/plus';
import JiessEnv from '@jiess/plus/lib/envReact';

// import { $entry } from '../../../../../jiess_2023_7_1/JIESS/FA/jiess-plus/src/index.js';
// import JiessEnv from '../../../../../jiess_2023_7_1/JIESS/FA/jiess-plus/src/env/react/index.js';

// 除了引入所需的组件，还需要手动引入组件样式
const jiess = $entry(JiessEnv,
  // 静态注入到jiess实例中
  {
    $defTag: View,
    useRef: React.useRef,
    useEffect: React.useEffect,
    useState: React.useState,
    useReducer: React.useReducer,
    // 首字母大写才会被视为组件
    $render(IS, param, children) {
      return <IS {...param}>{children}</IS>
    }
  });

export const JiessComponent = jiess.JiessComponent;
