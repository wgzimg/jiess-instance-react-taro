import { AtCard, AtButton, AtToast } from 'taro-ui';
import { JiessComponent } from '../../jiessConfig';
import { Text } from '@tarojs/components';
import { useLoad } from '@tarojs/taro';
import './index.scss';
import { $val, $ref } from '@jiess/plus';
// --------------------------------------------------
export default function Index() {
  useLoad(() => {
    console.log('Page loaded.')
  })
  return (
    <JiessComponent setup={function () {
      const show = $ref(false);
      this.add(
        this.render({
          is: AtToast,
          text: '这是一个吐司',
          isOpened: $val(show, 'value')
        }),
        this.render({
          is: AtCard,
          title: '这是标题'
        },
          this.render({ is: Text }, '你好 Jiess'),
          this.render({
            is: AtButton,
            type: 'primary',
            onClick: () => {
              show.value = true
              setTimeout(() => {
                show.value = false
              }, 2000)
            }
          }, '点击')
        )
      )
    }} />
  )
}
